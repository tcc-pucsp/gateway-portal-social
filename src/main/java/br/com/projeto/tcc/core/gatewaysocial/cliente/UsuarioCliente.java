package br.com.projeto.tcc.core.gatewaysocial.cliente;

import br.com.projeto.tcc.core.gatewaysocial.modelo.Login;
import br.com.projeto.tcc.core.gatewaysocial.modelo.Usuario;
import br.com.projeto.tcc.core.gatewaysocial.propriedade.PropriedadeSocial;
import br.com.projeto.tcc.core.gatewaysocial.recurso.UsuarioRecurso;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.net.URI;

@Slf4j
@Component
public class UsuarioCliente implements UsuarioRecurso {

    private final RestTemplate webClient;

    private final PropriedadeSocial propriedadeSocial;

    public UsuarioCliente(@Qualifier("restTemplate") RestTemplate webClient, PropriedadeSocial propriedadeSocial) {
        this.webClient = webClient;
        this.propriedadeSocial = propriedadeSocial;
    }

    @Override
    public Login loadUserByUsername(String cnpj) throws UsernameNotFoundException {

        final URI uri = URI.create(propriedadeSocial.getUrl() + "/social/login/" + cnpj);

        ResponseEntity<Login> responseEntity = webClient.getForEntity(uri, Login.class);

        if (responseEntity.getStatusCode().isError()) {
            throw new UsernameNotFoundException("Usuario não encontrado");
        }

        return responseEntity.getBody();

    }

    @Override
    public Usuario obterPorCnpj(String cnpj) {

        final URI uri = URI.create(propriedadeSocial.getUrl() + "/social/" + cnpj);

        ResponseEntity<Usuario> responseEntity = webClient.getForEntity(uri, Usuario.class);

        if (responseEntity.getStatusCode().isError()) {
            throw new UsernameNotFoundException("Usuario não encontrado");
        }

        return responseEntity.getBody();


    }

}
