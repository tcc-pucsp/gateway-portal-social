package br.com.projeto.tcc.core.gatewaysocial;

import br.com.projeto.tcc.core.gatewaysocial.propriedade.PropriedadeOrigem;
import br.com.projeto.tcc.core.gatewaysocial.propriedade.PropriedadeSocial;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;

@EnableZuulProxy
@SpringBootApplication
@EnableConfigurationProperties({PropriedadeSocial.class, PropriedadeOrigem.class})
public class GatewaySocialApplication {

	public static void main(String[] args) {
		SpringApplication.run(GatewaySocialApplication.class, args);
	}

}
