package br.com.projeto.tcc.core.gatewaysocial.configuracao;

import br.com.projeto.tcc.core.gatewaysocial.propriedade.PropriedadeOrigem;
import br.com.projeto.tcc.core.gatewaysocial.servico.LoginServico;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;

@Slf4j
@EnableWebSecurity
@EnableAuthorizationServer
@EnableResourceServer
public class SegurancaoConfiguracao extends WebSecurityConfigurerAdapter {

    private final LoginServico userDetailsService;

    private final PropriedadeOrigem propriedadeOrigem;

    public SegurancaoConfiguracao(LoginServico userDetailsService, PropriedadeOrigem propriedadeOrigem) {
        this.userDetailsService = userDetailsService;
        this.propriedadeOrigem = propriedadeOrigem;
    }

    @Bean
    @Override
    protected AuthenticationManager authenticationManager() throws Exception {
        return super.authenticationManager();
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailsService).passwordEncoder(passwordEncoder());
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }
    
    @Override
    public void configure(WebSecurity web) {
        web.ignoring().antMatchers(HttpMethod.POST, "/social");
    }
}
