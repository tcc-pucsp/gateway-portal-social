package br.com.projeto.tcc.core.gatewaysocial.propriedade;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

@Data
@ConfigurationProperties(prefix = "social")
public class PropriedadeSocial {

    private String url;

}
