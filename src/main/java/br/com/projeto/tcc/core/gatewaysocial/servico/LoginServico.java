package br.com.projeto.tcc.core.gatewaysocial.servico;

import br.com.projeto.tcc.core.gatewaysocial.recurso.UsuarioRecurso;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class LoginServico implements UserDetailsService {

    private final UsuarioRecurso usuarioRecurso;

    public LoginServico(UsuarioRecurso usuarioRecurso) {
        this.usuarioRecurso = usuarioRecurso;
    }

    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {

        return usuarioRecurso
                .loadUserByUsername(s);
    }
}
