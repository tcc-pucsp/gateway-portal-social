package br.com.projeto.tcc.core.gatewaysocial.filtro;

import br.com.projeto.tcc.core.gatewaysocial.modelo.Usuario;
import br.com.projeto.tcc.core.gatewaysocial.recurso.UsuarioRecurso;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.Optional;

@Slf4j
@Component
public class BordaInjecao implements Filter {

    @Value("${security.oauth2.client.client-id}")
    private String clientId;

    private final UsuarioRecurso usuarioRecurso;

    public BordaInjecao(UsuarioRecurso usuarioRecurso) {
        this.usuarioRecurso = usuarioRecurso;
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {

        final HttpServletRequest httpRequest = (HttpServletRequest) servletRequest;

        final String cnpj = Optional.ofNullable(((HttpServletRequest) servletRequest).getUserPrincipal()).orElse(() -> clientId).getName();

        if (cnpj != null && !cnpj.equals(clientId)) {

            final Usuario usuario = usuarioRecurso.obterPorCnpj(cnpj);

            MutableHttpServletRequest mutableHttpServletRequest = usuario.obterRequestComHeadersInjetados(httpRequest);

            filterChain.doFilter(mutableHttpServletRequest, servletResponse);

        } else {

            filterChain.doFilter(servletRequest, servletResponse);

        }

    }
}

