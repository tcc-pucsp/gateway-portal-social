package br.com.projeto.tcc.core.gatewaysocial.propriedade;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

@Data
@ConfigurationProperties(prefix = "seguranca")
public class PropriedadeOrigem {

    private String origin;

}
