package br.com.projeto.tcc.core.gatewaysocial.modelo;

import br.com.projeto.tcc.core.gatewaysocial.filtro.MutableHttpServletRequest;
import lombok.Value;

import javax.servlet.http.HttpServletRequest;

@Value
public class Usuario {

    Integer id;

    String email;

    Endereco endereco;

    @Value
    private static class Endereco {

        String latitude;

        String longitude;

    }

    public MutableHttpServletRequest obterRequestComHeadersInjetados(HttpServletRequest request) {

        MutableHttpServletRequest mutableRequest = new MutableHttpServletRequest(request);

        mutableRequest.addHeader("id-social", id.toString());
        mutableRequest.addHeader("email-social", email);
        mutableRequest.addHeader("latitude", endereco.getLatitude());
        mutableRequest.addHeader("longitude", endereco.getLongitude());

        return mutableRequest;

    }
}
