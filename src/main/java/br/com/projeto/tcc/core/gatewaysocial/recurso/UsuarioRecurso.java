package br.com.projeto.tcc.core.gatewaysocial.recurso;

import br.com.projeto.tcc.core.gatewaysocial.modelo.Login;
import br.com.projeto.tcc.core.gatewaysocial.modelo.Usuario;

public interface UsuarioRecurso {

    Login loadUserByUsername(String cnpj);

    Usuario obterPorCnpj(String cnpj);

}
